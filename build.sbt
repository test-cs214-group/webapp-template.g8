lazy val webappTemplateG8 = (project in file(".")).
  settings(
    scalaVersion := "3.3.1",
    name := "webappTemplateG8",
    resolvers += Resolver.url("typesafe", url("https://repo.typesafe.com/typesafe/ivy-releases/"))(Resolver.ivyStylePatterns)
  )