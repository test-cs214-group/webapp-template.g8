package apps

import cs214.webapp.server.{ApplicationJVM, WebServer}


object MainJVM extends ApplicationJVM:
  @main def main() = this.start()