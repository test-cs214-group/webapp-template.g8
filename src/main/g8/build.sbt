import com.github.sbt.jacoco.JacocoKeys.jacocoAggregateReportSettings

/// Commands

lazy val copyJsTask = TaskKey[Unit]("copyJsTask", "Copy javascript files to server target directory")
lazy val copyTestReportsTask = TaskKey[Unit]("copyTestReportsTask", "Copy test reports files to root target directory")

/// Tests

lazy val apps = (crossProject(JVMPlatform, JSPlatform) in file("./apps"))
  .settings(
    name := "apps",
    version := "0.1.0-SNAPSHOT",
    scalaVersion := "3.3.1",
    scalacOptions ++= Seq("-deprecation")
  ).jsSettings(
    scalaJSUseMainModuleInitializer := true,
    test / aggregate := false,
    Test / test := {},
    Test / testOnly := {},
    libraryDependencies ++= Seq(
      "com.lihaoyi" %%% "ujson" % "3.1.3",
      "org.scala-js" %%% "scalajs-dom" % "2.8.0",
      "com.lihaoyi" %%% "scalatags" % "0.12.0",
    )
  ).jvmSettings(
    run / fork := true,
    Global / cancelable := true,
    libraryDependencies ++= Seq(
      "org.java-websocket" % "Java-WebSocket" % "1.5.4",
      "org.scala-lang" %% "toolkit-test" % "0.1.7" % Test,
      "com.lihaoyi" %% "ujson" % "3.1.3",
      dependencies.websocket,
      dependencies.http4sCircle,
      dependencies.http4sDsl,
      dependencies.http4sEmberServer,
      dependencies.circle,
      dependencies.slf4j,
    )
  )

/// Dependencies

//=========   ====  == =
//      WEBAPP LIBRARY RESOLUTION
//=========   ====  == =

val webappLibRepo = "git://gitlab.epfl.ch/lamp/cs-214/webapp-lib.git"
val resolveWebappLibLocally = true

lazy val client: ClasspathDep[ProjectReference] =
  if (resolveWebappLibLocally) { project in file( "./../webapp-lib") }
  else { ProjectRef(uri(webappLibRepo), "client") }

lazy val server: ClasspathDep[ProjectReference] =
  if (resolveWebappLibLocally) { project in file("./../webapp-lib") }
  else { ProjectRef(uri(webappLibRepo), "server") }

//=========   ====  == =

lazy val appsJS = apps.js.dependsOn(client)
lazy val appsJVM = apps.jvm.dependsOn(server)

/// Aggregate project

lazy val webapp = (project in file("."))
  .aggregate(
    appsJS, appsJVM,
  ).settings(
    name := "webapp",
    scalaVersion := "3.3.1",
    scalacOptions ++= Seq("-deprecation"),
    copyJsTask := {
      println("[info] Copying generated main.js to server's static files directory...")
      val inDir = baseDirectory.value / "apps/js/target/scala-3.3.1/apps-fastopt/"
      val outDir = baseDirectory.value / "apps/jvm/src/main/resources/www/static/"
      Seq("main.js", "main.js.map") map { p => (inDir / p, outDir / p) } foreach { f => IO.copyFile(f._1, f._2) }
    },
    copyTestReportsTask := {
      println("[info] Copying test reports to lab root directory...")
      val inDir = baseDirectory.value / "apps/jvm/target/test-reports/"
      val outDir = baseDirectory.value / "target/test-reports/"
      IO.copyDirectory(inDir, outDir, CopyOptions(overwrite = true, preserveLastModified = false, preserveExecutable = false))
    },
    jacocoAggregateReportSettings := JacocoReportSettings(
      title = "Webapp Project Coverage",
      formats = Seq(JacocoReportFormats.ScalaHTML)
    ),
    appsJVM / Test / testOnly := {
      (appsJVM / Test / testOnly).parsed.doFinally(copyTestReportsTask.taskValue).value
    },
    Test / test := Def.sequential(
      (Test / testOnly).toTask("")
    ).value,
    Compile / run := Def.sequential(
      Compile / compile,
      (appsJS / Compile / fastLinkJS).toTask,
      copyJsTask,
      (appsJVM / Compile / run).toTask(""),
    ).value,
  )

/// Dependencies

lazy val dependencies =
  new {
    val http4sVersion = "1.0.0-M39"
    val circeVersion = "0.14.5"
    val ujson = "com.lihaoyi" %% "ujson" % "3.1.3"
    // Websocket dependencies
    val websocket = "org.java-websocket" % "Java-WebSocket" % "1.5.4"
    // Http dependencies
    val http4sEmberServer = "org.http4s" %% "http4s-ember-server" % http4sVersion
    val http4sDsl = "org.http4s" %% "http4s-dsl" % http4sVersion
    val http4sCircle = "org.http4s" %% "http4s-circe" % http4sVersion
    val circle = "io.circe" %% "circe-generic" % circeVersion
    val slf4j = "org.slf4j" % "slf4j-nop" % "2.0.5"
  }
